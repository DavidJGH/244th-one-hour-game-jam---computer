﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Window : MonoBehaviour
{
    private Rigidbody2D _rb;
    private Collider2D _close;
    
    void Start()
    {
        _rb = transform.GetComponent<Rigidbody2D>();
        _close = transform.GetComponents<BoxCollider2D>().Last();
    }

    private Vector2 offset;
    void OnMouseDown()
    {
        offset = Camera.main.ScreenToWorldPoint(Input.mousePosition)-transform.position;
    }

    private void OnMouseDrag()
    {
        _rb.MovePosition((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - offset);
    }

    private void OnMouseUp()
    {
        if(_close.OverlapPoint((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition))) Destroy(gameObject);
    }
}
