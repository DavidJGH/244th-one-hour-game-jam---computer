﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class App : MonoBehaviour
{
    public GameObject window;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        GameObject instance = Instantiate(window, transform.parent.parent.parent, true);
        instance.transform.localScale = new Vector2(.75f, .75f);
        int sortingOrder = transform.parent.parent.GetComponent<Canvas>().sortingOrder + 1;
        instance.GetComponent<SpriteRenderer>().sortingOrder = sortingOrder;
        instance.transform.GetChild(0).GetComponent<Canvas>().sortingOrder = sortingOrder + 1;
    }
}
